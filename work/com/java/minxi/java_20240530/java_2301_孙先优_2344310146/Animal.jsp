<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>页面标题</title>
</head>
<body>
<p>
<%!
    class Animal{
        String sleep(int time){
            return "一只动物在睡觉";
        }
    }
    class Cat extends Animal{
        private String name;
        private String time;

        public Cat() {
        }

        public Cat(String name, String time) {
            this.name = name;
            this.time = time;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        String sleep(int time){

            return "一只"+getName()+"睡觉睡了"+getTime()+"个小时";
        }
    }

%>
<%
    Animal animal = new Animal();
    String sleep1 = animal.sleep(8);

    Animal cat = new Cat("汤姆猫","8");
    String sleep2 = cat.sleep(8);
%>
<br><%=sleep1%>
<br><%=sleep2%>
</p>
</body>
</html>