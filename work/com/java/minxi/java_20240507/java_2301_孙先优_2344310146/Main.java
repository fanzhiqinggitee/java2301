package com.java.minxi.java_20240507.java_2301_孙先优_2344310146;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入公交车采用的刹车技术和车票价（元）");

        Bus bus=new Bus();
        bus.brake(sc.next());
        bus.charge(sc.next());
        System.out.println("请输入出租车采用的刹车技术、车票价（元）、安装的空调样式");
        Taxi taxi=new Taxi();
        taxi.brake(sc.next());
        taxi.charge(sc.next());
        taxi.controlAirTemperature(sc.next());

    }
}
