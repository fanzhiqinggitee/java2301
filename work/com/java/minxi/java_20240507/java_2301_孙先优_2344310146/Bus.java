package com.java.minxi.java_20240507.java_2301_孙先优_2344310146;

public class Bus extends MotorVehicles implements MoneyFare{
    @Override
    public void charge(String c) {
        System.out.println("公共汽车:"+c+"元/张，不计算公里数");
    }

    @Override
    public void brake(String s) {
        System.out.println("公共汽车使用刹车技术："+ s);

    }
}
