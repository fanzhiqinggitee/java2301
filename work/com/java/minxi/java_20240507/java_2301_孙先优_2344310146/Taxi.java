package com.java.minxi.java_20240507.java_2301_孙先优_2344310146;

public  class Taxi extends MotorVehicles implements MoneyFare,ControlTemperature{
    @Override
    public void controlAirTemperature(String t) {
        System.out.println("出租车安装了"+t+"空调");
    }

    @Override
    public void charge(String c) {
        System.out.println("出租车:"+c+"元/公里，起步价3公里");
    }

    @Override
    public void brake(String s) {
        System.out.println("出租车使用刹车技术："+ s);

    }
}
