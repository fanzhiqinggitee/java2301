package com.java.minxi.java_20240428.java_2301_孙先优_2344310146;

public class Person {
    //    1）.声明一个名为Person的类，里面有name与age两个属性，并声明一个含有两个参数的构造方法；
//            2）.声明一个名为Student的类，此类继承自Person类，添加一个属性school；
//            3）.在Student类的构造方法中调用父类中有两个参数的构造方法；
//            4）.定义一个intro方法，进行自我介绍，输出格式：姓名：张三，年龄：18，
//            学校：闽西职业技术学院。注意：输出语句中“，”和“：”均为中文标点符号。
//            5）.实例化一个Student类的对象student，为Student对象student中的school赋值，
//            调用intro方法，打印输出姓名：张三，年龄：18，学校：龙岩学院。
    private String name;
    private int age;


    public Person() {

    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


}
