package com.java.minxi.java_20240428.java_2301_孙先优_2344310146;

public class Student extends Person{
    private String school;

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public Student(String school) {
        this.school = school;
    }

    public Student(String name, int age, String school) {
        super(name, age);
        this.school = school;
    }
    public void intro() {
        System.out.println("姓名："  + getName() + "，" + "年龄：" + +getAge()  + "，" + "学校：" +  school );
    }

}
