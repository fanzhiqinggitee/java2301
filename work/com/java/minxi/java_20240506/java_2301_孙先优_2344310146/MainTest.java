package com.java.minxi.java_20240506.java_2301_孙先优_2344310146;

public class MainTest {
    public static void main(String[] args) {
        Vehicles vl = new Car();
        vl.setColor("白色");
        vl.setBrand("大众");
        vl.setType("汽车");
        vl.setAddress("北京");
        vl.travel();


        Vehicles vl1 = new Car();
        vl1.setColor("");
        vl1.setBrand("奔驰");
        vl1.setType("汽车");
        vl1.setAddress("东山岛");
        vl1.travel();

        Vehicles vl2 = new AirPlane();
        vl2.setColor("蓝色");
        vl2.setBrand("波音");
        vl2.setType("客机");
        vl2.setAddress("大理");
        vl2.travel();

        Vehicles vl3 = new AirPlane();
        vl3.setColor("");
        vl3.setBrand("空客");
        vl3.setType("客机");
        vl3.setAddress("哈尔滨岛");
        vl3.travel();


    }
}
