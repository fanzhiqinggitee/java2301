package com.java.minxi.java_20240506.java_2301_孙先优_2344310146;

public class Vehicles {
    private String type;
    private String brand;
    private String color;
    private String address;

    public Vehicles() {
    }

    public Vehicles(String type, String brand, String color, String address) {
        this.type = type;
        this.brand = brand;
        this.color = color;
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public void travel(){
        System.out.println();
    }
}
