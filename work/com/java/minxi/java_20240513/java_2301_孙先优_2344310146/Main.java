package com.java.minxi.java_20240513.java_2301_孙先优_2344310146;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        //  1、  创建一个`ArrayList`，初始化它，并添加5个字符串元素。
        System.out.println("创建和初始化：");
        ArrayList arraylist = new ArrayList();
        arraylist.add("arr1");
        arraylist.add("arr2");
        arraylist.add("arr3");
        arraylist.add("arr4");
        arraylist.add("arr5");
        System.out.println(arraylist);

        //  2、 在`ArrayList`中添加一个元素到列表的开头。
        System.out.println("添加元素：");
        System.out.println("修改前:" + arraylist);
        arraylist.add(0, "arr0");
        System.out.println("修改后:" + arraylist);

        System.out.println("===================");

        // 3、从`ArrayList`中删除索引为2的元素。
        System.out.println("删除元素：");
        System.out.println("修改前:" + arraylist);
        arraylist.remove(2);
        System.out.println("修改后:" + arraylist);

        System.out.println("===================");

        //4、 获取`ArrayList`中索引为3的元素。
        System.out.println("访问元素：");
        System.out.println("修改前:" + arraylist);
        System.out.println("修改后:" + arraylist.get(3));

        System.out.println("===================");

        //5、 将`ArrayList`中索引为1的元素修改为"New Element"。
        System.out.println("修改元素：");
        System.out.println("修改前:" + arraylist);
        arraylist.set(1, "New Element");
        System.out.println("修改后:" + arraylist);

        System.out.println("===================");

        // 6、  在`ArrayList`中搜索字符串"Target"的位置。
        System.out.println("元素搜索：");
        System.out.println("修改前:" + arraylist);
        if (arraylist.contains("Target")) {
            System.out.println("修改后：" + "Target在arraylist中的位置：" + arraylist.indexOf("Target"));
        } else {
            System.out.println("修改后：" + "Target不在ArrayList中");
        }


        System.out.println("===================");


        //7、将`ArrayList`中所有出现的"Old"字符串替换为"New"。
        System.out.println("元素替换：");
        System.out.println("修改前:" + arraylist);
        arraylist.replaceAll(a->a.equals("old")?"New":a);
        System.out.println("修改后:" + arraylist);

        System.out.println("===================");

        //8. 反转`ArrayList`中的元素顺序。
        System.out.println("列表反转：");
        System.out.println("修改前:" + arraylist);
        Collections.reverse(arraylist);
        System.out.println("修改后:" + arraylist);

        System.out.println("===================");

        //9. 对`ArrayList`中的字符串元素进行排序
        System.out.println("列表排序：");
        System.out.println("修改前:" + arraylist);
        Collections.sort(arraylist);
        System.out.println("修改后:" + arraylist);

        System.out.println("===================");

        //10. 比较两个`ArrayList`是否相等。
        System.out.println("列表比较：");
        System.out.println("修改前:" + arraylist);
        ArrayList<Object> objects = new ArrayList<>();
        objects.add("q");
        objects.add("w");
        objects.add("e");
        objects.add("r");
        objects.add("t");
        if (arraylist.equals(objects)){
            System.out.println("修改后："+"arraylist与objects相同");
        }else {
            System.out.println("修改后："+"arraylist与objects不相同");
        }


    }


}
