package com.java.minxi.java_20240513.java_2301_孙先优_2344310146;

import java.util.Collections;
import java.util.LinkedList;

public class Main1 {
    public static void main(String[] args) {
        //1、创建一个`LinkedList`，初始化它，并添加5个整数元素。
        System.out.println("创建和初始化：");
        LinkedList linkedList = new LinkedList();
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(4);
        linkedList.add(5);
        System.out.println(linkedList);

        //2、 在`LinkedList`中添加一个元素到列表的末尾。
        System.out.println("添加元素：");
        System.out.println("修改前:" + linkedList);
        linkedList.addLast(6);
        System.out.println(linkedList);
        System.out.println("修改后:" + linkedList);

        System.out.println("================");

        //3、 从`LinkedList`中删除第一个元素。
        System.out.println("删除元素：");
        System.out.println("修改前:" + linkedList);
        linkedList.remove(0);
        System.out.println(linkedList);
        System.out.println("修改后:" + linkedList);

        System.out.println("================");

        //4、获取`LinkedList`中最后一个元素。
        System.out.println("访问元素：");
        System.out.println("修改前:" + linkedList);
        System.out.println(linkedList.getLast());
        System.out.println("修改后:" + linkedList);

        System.out.println("================");


        //5、将`LinkedList`中索引为0的元素修改为42。
        System.out.println("修改元素：");
        System.out.println("修改前:" + linkedList);
        linkedList.set(0, 42);
        System.out.println(linkedList);
        System.out.println("修改后:" + linkedList);

        System.out.println("================");

        //  6、  在`LinkedList`中搜索整数42的位置。
        System.out.println("元素搜索：");
        System.out.println("修改前:" + linkedList);
        if (linkedList.contains(42)) {
            System.out.println("修改后："+"整数42的位置在索引：" + linkedList.indexOf(42));
        } else {
            System.out.println("修改后："+42 + "不在LinkedList中");
        }

        System.out.println("================");

        //7、 将`LinkedList`中所有出现的数字10替换为20。
        System.out.println("元素替换：");
        System.out.println("修改前:" + linkedList);
        linkedList.replaceAll(a->a.equals(10)?20:a);
        System.out.println("修改后:" + linkedList);

        System.out.println("================");




        //8、反转`LinkedList`中的元素顺序。
        System.out.println("列表反转：");
        System.out.println("修改前:" + linkedList);
        Collections.reverse(linkedList);
        System.out.println("修改后:" + linkedList);

        System.out.println("================");

        //9、对`LinkedList`中的整数元素进行排序。
        System.out.println("列表排序：");
        System.out.println("修改前:" + linkedList);
        Collections.sort(linkedList);
        System.out.println("修改后:" + linkedList);

        System.out.println("================");

        //10、比较两个`LinkedList`是否相等。
        System.out.println("列表比较：");
        System.out.println("修改前:" + linkedList);
        LinkedList<Object> objects = new LinkedList<>();
        objects.add(10);
        objects.add(42);
        objects.add(44);
        objects.add(14);
        objects.add(67);
        if (linkedList.equals(objects)){
            System.out.println("修改后："+"linkedList与objects相同");
        }else {
            System.out.println("修改后："+"linkedList与objects不相同");
        }
    }
}
