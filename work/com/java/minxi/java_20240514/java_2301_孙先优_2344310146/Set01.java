package com.java.minxi.java_20240514.java_2301_孙先优_2344310146;
import java.util.HashSet;
public class Set01 {
    public static void main(String[] args) {
        //创建一个 `HashSet` 并添加至少5个不同的字符串元素。
        System.out.println("1、创建和添加元素");
        HashSet<Object> objects = new HashSet<>();
        objects.add("q");
        objects.add("w");
        objects.add("e");
        objects.add("r");
        objects.add("t");
        System.out.println(objects);

        System.out.println("================");

        //检查一个特定的字符串是否已经存在于你创建的 `HashSet` 中。
        System.out.println("2、元素检查");
        System.out.println("修改前：" + objects);
        System.out.println("修改后：");
        System.out.println("objects是否存在字符串q:" + objects.contains("q"));

        System.out.println("================");

        //从 `HashSet` 中移除一个已存在的元素，并确认它已被移除。
        System.out.println("3、元素移除");
        System.out.println("修改前：" + objects);
        objects.remove("w");
        System.out.println("修改后：");
        System.out.println("移除元素后：" + objects);

        System.out.println("================");

        //打印出 `HashSet` 的大小。
        System.out.println("4、集合大小");
        System.out.println("修改前：" + objects);
        System.out.println("修改后：");
        System.out.println("集合的大小为：" + objects.size());

        System.out.println("================");

        //使用增强型 `for` 循环遍历 `HashSet` 并打印每个元素。
        System.out.println("5、集合遍历");
        System.out.println("修改前：" + objects);
        System.out.println("修改后：");
        for (Object object : objects) {
            System.out.println(object);
        }

        System.out.println("================");

        //将 `HashSet` 转换为数组，并打印数组中的每个元素。
        System.out.println("6、集合转换为数组");
        System.out.println("修改前：" + objects);
        System.out.println("修改后：");
        String[] arr = objects.toArray(new String[0]);
        for (String s : arr) {
            System.out.println(s);
        }

        System.out.println("================");

        //创建两个 `HashSet` 对象，实现它们的并集操作。
        System.out.println("7、集合的并集");
        HashSet<Object> objects1 = new HashSet<>();
        objects1.add(1);
        objects1.add(2);
        objects1.add(3);

        HashSet<Object> objects2 = new HashSet<>();
        objects2.add(4);
        objects2.add(5);
        objects2.add(6);

        HashSet<Object> objects3 = new HashSet<>();
        objects3.addAll(objects1);
        objects3.addAll(objects2);
        System.out.println(objects3);

        System.out.println("================");

        //对上面创建的两个 `HashSet` 对象实现交集操作。
        System.out.println("8、集合的交集");
        HashSet<Object> objects4 = new HashSet<>();
        objects4.addAll(objects1);
        objects4.retainAll(objects2);
        System.out.println(objects4);

        System.out.println("================");

        //1. 对上面创建的两个 `HashSet` 对象实现差集操作。
        System.out.println("9、集合的差集");
        HashSet<Object> objects5 = new HashSet<>();
        objects5.addAll(objects1);
        objects5.removeAll(objects2);
        System.out.println(objects5);

    }
}
