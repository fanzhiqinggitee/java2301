package com.java.minxi.java_20240514.java_2301_孙先优_2344310146;

public class HashMap01 {
    private String name;
    private int age;

    public HashMap01() {
    }

    public HashMap01(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "HashMap01{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
