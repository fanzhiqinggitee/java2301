package com.java.minxi.java_20240514.java_2301_孙先优_2344310146;
import java.util.HashMap;
import java.util.Map;
public class Map01 {
        public static void main(String[] args) {
//        1. **创建和添加键值对**
//        创建一个 `HashMap` 并添加至少3对键值对，键可以是字符串，值可以是整数。
            HashMap<String, Integer> map = new HashMap<>();
            map.put("q", 1);
            map.put("w", 2);
            map.put("e", 3);
            System.out.println(map);

            System.out.println("================");

//        2. **获取和打印值**
//        使用键来获取对应的值，并打印出来。
            System.out.println(map.get("q"));
            System.out.println(map.get("w"));
            System.out.println(map.get("e"));

            System.out.println("================");

//        3. **检查键是否存在**
//        检查一个特定的键是否存在于 `HashMap` 中。
            System.out.println("检查一个特定的键是否存在于 `HashMap` 中：true是在，flase是不在");
            System.out.println(map.containsKey("q"));

            System.out.println("================");

//        4. **键集合遍历**
//        遍历 `HashMap` 的键集合，并打印每个键。
            for (String o : map.keySet()) {
                System.out.println(o);
            }
            System.out.println("================");


//        5. **值集合遍历**
//        遍历 `HashMap` 的值集合，并打印每个值。
            for (Object value : map.values()) {
                System.out.println(value);
            }
            System.out.println("================");
//        6. **键值对遍历**
//        使用 `entrySet()` 遍历 `HashMap` 的所有键值对，并打印。
            for (Map.Entry<String, Integer> stringObjectEntry : map.entrySet()) {
                System.out.println(stringObjectEntry);
            }
            System.out.println("================");
//        7. **更新值**
//        对一个已存在的键更新其对应的值。
            map.put("q", 67);
            System.out.println(map);
            System.out.println("================");
//        8. **移除键值对**
//        移除一个特定的键值对，并确认它已被移除。
            System.out.println("如果成功移除打印true，如果未移除则打印flase");
            System.out.println(map.remove("q", 67));

            System.out.println("================");

//        9. **Map 的大小**
//        打印出 `HashMap` 的大小。
            System.out.println("map的大小为：" + map.size());
            System.out.println("================");

//        10. **使用泛型**
//        创建一个泛型 `HashMap`，键为自定义对象，值为字符串，并添加键值对。

            HashMap<HashMap01, String> h1 = new HashMap<>();
            h1.put(new HashMap01("张三",18),"2");
            h1.put(new HashMap01("李四",20),"1");
            for (Map.Entry<HashMap01, String> en : h1.entrySet()) {
                System.out.println(en.getKey().getName()+","+en.getKey().getAge()+","+en.getValue());
            }
        }
}
