package com.java.minxi.java_20240516.java_2301_孙先优_2344310146;

public class Person {
//    1. 重写 `toString` 方法
//   - 创建一个 `Person` 类，包含 `name` 和 `age` 属性。
//    重写 `toString` 方法，使其返回一个字符串，包含对象的 `name` 和 `age`。
    private String name;
    private int age;

    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Person person=(Person) obj;
        return age==person.age&&(person.name!=null?
              name.equals(person.name):name==null);
    }



    }


