package com.java.minxi.java_20240516.java_2301_孙先优_2344310146;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;
public class MainTest001 {
    public static void main(String[] args) throws ParseException {
        // **`Object` 类练习题**：
        // 1. 重写 `toString` 方法
        //         - 创建一个 `Person` 类，包含 `name` 和 `age` 属性。重写 `toString` 方法，使其返回一个字符串，包含对象的 `name` 和 `age`
        Person person = new Person("翠花", 25);
        System.out.println(person.getName()+person.getAge());

        System.out.println("---------------------------------");

        // 2. 使用 `equals` 方法
        //         - 在 `Person` 类中重写 `equals` 方法，使得可以通过 `name` 和 `age` 属性比较两个 `Person` 对象是否相等。
        Person person_1 = new Person("张三", 19);
        Person person_2 = new Person("张三", 19);
        System.out.println("person_1与person_2是否相等：" + person_1.equals(person_2));

        System.out.println("---------------------------------");

        // **`Math` 类练习题**：
        // 1. 绝对值
        //         - 编写一个程序，接受一个用户输入的数字，然后输出该数字的绝对值。
        System.out.println("请输入一个整数:");
        Scanner sc = new Scanner(System.in);
        int i = sc.nextInt();
        System.out.println("该数字的绝对值是：" + Math.abs(i));

        System.out.println("---------------------------------");

        // 2. 最小值和最大值
        //         - 创建一个方法，接受三个整数作为参数，返回这三个整数中的最小值和最大值。
        System.out.println("三个数字分别是：5，6，9");
        System.out.println(Number(5,6,9));

        System.out.println("---------------------------------");

        // 3. 四舍五入
        //         - 编写一个程序，接受一个浮点数作为输入，并将其四舍五入到最近的整数。
        System.out.println("请输入一个浮点数:");
        Double db = sc.nextDouble();
        System.out.println(db + "四舍五入是：" + Math.round(db));
        
        System.out.println("---------------------------------");

        // 4. 随机数生成
        //         - 使用 `Math.random()` 方法生成一个 0 到 1 之间的随机数，并将其乘以一个用户指定的输入值。
        System.out.println("请输入一个数字:");
        int it = sc.nextInt();
        double random = Math.random();
        System.out.println(it + "*" + random + "=" +( it * random));

        System.out.println("---------------------------------");

        // 5. 计算平均值
        //         - 创建一个方法，接受一个整数数组，并返回数组的平均值。
        int[] arr = {1, 4, 10, 20, 40, 50};
        System.out.println("整数数组：" + Arrays.toString(arr));
        System.out.println("数组的平均值是：" + Avg(arr));

        System.out.println("---------------------------------");

        // **`Date` 类练习题**：
        // 1. 创建 Date 对象
        //         - 编写一个程序，创建一个 `Date` 对象，表示当前时间。
        Date date = new Date();
        System.out.println("当前时间是：" + date);

        System.out.println("---------------------------------");

        // 2. 获取 Date 对象的年份、月份、天数等
        //         - 创建一个方法，接受一个 `Date` 对象作为参数，返回该日期的年份、月份和天数。
        Date date2 = new Date();
        Time(date2);

        System.out.println("---------------------------------");

        // 3. 格式化 Date 对象
        //         - 使用 `SimpleDateFormat` 类将 `Date` 对象格式化为指定的日期格式字符串，如 "yyyy-MM-dd HH:mm:ss"。
        Date date3 = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(sdf.format(date3));

        System.out.println("---------------------------------");

        // 4. Date 转字符串再转回 Date
        //         - 实现一个程序，将 `Date` 对象转换为字符串，然后将该字符串重新解析回 `Date` 对象。
        Date date4 = new Date();
        String time = sdf.format(date4);
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(sdf2.parse(time));

        System.out.println("---------------------------------");

        // 5. System类使用
        //         - 使用System类获得当前时间戳，将时间戳转换成`Date` 对象，要求按照该格式 "yyyy-MM-dd HH:mm:ss"  打印出当前时间。
        long time2 = System.currentTimeMillis();
        Date date5 = new Date();
        date5.setTime(time2);
        SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(simpleDateFormat3.format(date5));

        System.out.println("---------------------------------");

        // `String` 类练习题：
        // 1. 字符串连接
        //         - 编写一个程序，连接两个字符串，并输出结果。
        System.out.println("请输入第一个字符串：");
        String s1 = sc.next();
        System.out.println("请输入第二个字符串：");
        String s2 = sc.next();
        String s3 = s1 + s2;
        System.out.println("连接两个字符串后：" + s3);

        System.out.println("---------------------------------");

        // 2. 字符串长度
        //         - 创建一个方法，接受一个字符串作为参数，返回该字符串的长度。
        String s4 = "叫我优总";
        System.out.println("字符串为：" + s4);
        System.out.println(s4 + "长度为：" + StringSize(s4));

        System.out.println("---------------------------------");

        // 3. 字符串分割
        //         - 编写一个程序，将一个以逗号分隔的字符串分割成单词数组。
        System.out.println("请输入字符串：");
        String s5 = sc.next();
        String[] list = s5.split(",");
        System.out.println("分割的数组为：" + Arrays.toString(list));

        System.out.println("---------------------------------");

        // 4. 字符串反转
        //         - 实现一个方法，接受一个字符串作为参数，并返回其反转后的字符串。
        String s6 = "我爱你";
        StringReverse(s6);

        System.out.println("---------------------------------");

        // 5. 字符串大小写转换
        //         - 创建一个方法，将输入的字符串转换为全小写或全大写。
        System.out.println("请输入英文：");
        String s7 = sc.next();
        StringLowerUp(s7);

        System.out.println("---------------------------------");

        // 6. 字符串搜索
        //         - 编写一个程序，检查一个字符串是否包含另一个字符串。
        String s8 = "优哥最帅优哥最man";
        String s9 = "man";
        System.out.println("字符串1：" + s8);
        System.out.println("字符串2：" + s9);
        if (!s8.contains(s9)) {
            System.out.println("字符串1不包含字符串2");
        } else {
            System.out.println("字符串1包含字符串2");
        }

        System.out.println("---------------------------------");

        // 7. 字符串替换
        //         - 实现一个方法，将字符串中的所有指定字符替换为另一个字符。
        String s10 = "快点把我替换掉";
        System.out.println("字符串替换前：" + s10);
        System.out.println("字符串替换后：" + StringReplace(s10));

        System.out.println("---------------------------------");

        // 8. 字符串修剪
        //         - 创建一个方法，去除字符串两端的空白字符。
        String s11 = " 去除我两端的空白字符  ";
        System.out.println("去除两端空白前：" + s11);
        System.out.println("去除两端空白后：" + StringTrim(s11));

        System.out.println("---------------------------------");

        // 9. 字符串比较
        //         - 编写一个程序，比较两个字符串是否相等，忽略大小写。
        String s12 = "b j w m l g h l d x";
        String s13 = "B J W M L G H L D X";
        System.out.println("字符串1：" + s12);
        System.out.println("字符串2：" + s13);

        if (s12.equalsIgnoreCase(s13)) {
            System.out.println(s12 + "与" + s13 + "相等");
        } else {
            System.out.println(s12 + "与" + s13 + "不相等");
        }

        System.out.println("---------------------------------");

        // 10. 子串操作
        //         - 实现一个方法，提取一个字符串中指定范围内的子串。
        String s14 = "我爱你Java";
        System.out.println("字符串为：" + s14);
        System.out.println("提取前5个字串：" + StringInto(s14,5));

        System.out.println("---------------------------------");

        // 11. 字符串结束符检查
        //         - 编写一个程序，检查一个字符串是否以特定的后缀结束。
        String s15 = "我的后缀是啦啦啦";
        System.out.println("字符串为：" + s15);
        System.out.println("字符串是否以（啦啦啦）结束：" + s15.endsWith("啦啦啦"));

        System.out.println("---------------------------------");

        // 12. 字符串开始符检查
        //         - 实现一个方法，检查一个字符串是否以特定的前缀开始。
        String s16 = "我不爱你你不爱我";
        System.out.println("字符串为：" + s16);
        System.out.println("字符串是否以（我）结束：" + StringStar(s16));

        System.out.println("---------------------------------");

        // 13. 字符串中字符的出现次数
        //         - 编写一个程序，计算一个字符在字符串中出现的次数。
        String s17 = "我爱你你爱我";
        System.out.println("字符串为：" + s17);
        char searchChar = '爱';
        int count = 0;
        char[] array = s17.toCharArray();
        for (char j : array) {
            if (j == searchChar) {
                count++;
            }
        }
        System.out.println("字符" + searchChar + "出现的次数为：" + count);

        System.out.println("---------------------------------");

        // 14. 字符串与数字的转换
        //         - 创建一个方法，将字符串转换为相应的整数或浮点数，反之亦然。
        String s18 = "250";
        System.out.println("字符串为：" + s18);
        StringtoNumbers(s18);

        System.out.println("---------------------------------");

        // 15. StringBuilder拼接
        //         - 自定义三个字符串常量，使用 StringBuilder将这三个字符串拼接并输出。
        String s19 = "我有一头";
        String s20 = "小毛驴";
        String s21 = "我从来也不骑";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(s19).append(s20).append(s21);
        System.out.println("三个字符串拼接后：" + stringBuilder);

        System.out.println("---------------------------------");

        // **`BigDecimal` 类练习题：**
        // 1. 创建 BigDecimal 对象
        //         - 编写一个程序，使用字符串构造函数创建一个 `BigDecimal` 对象。
        BigDecimal bigDecimal = new BigDecimal(new String("666"));
        System.out.println("BigDecimal对象为：" + bigDecimal);
        System.out.println("---------------------------------");

        // 2. 基本算术运算
        //         - 实现一个方法，接受两个 `BigDecimal` 对象作为参数，执行加、减、乘、除运算。
        BigDecimal a = new BigDecimal(100);
        BigDecimal b = new BigDecimal(200);
        Big(a, b);
        System.out.println("---------------------------------");

        // 3. 舍入操作
        //         - 编写一个程序，对一个 `BigDecimal` 对象执行舍入操作，指定舍入模式和精度。
        BigDecimal c = new BigDecimal("0.086");
        System.out.println("请输入指定舍入模式（UP/DOWN）和精度（数字）(空格分开)：");
        String i3 = sc.next();
        int i4 = sc.nextInt();
        BigDecimal scale = c.setScale(i4, RoundingMode.valueOf(i3));
        System.out.println(scale);
        System.out.println("---------------------------------");

        // 4. 比较 BigDecimal 对象值
        //         - 创建一个方法，比较两个 `BigDecimal` 对象的值是否相等。
        BigDecimal d = new BigDecimal("10086");
        BigDecimal e = new BigDecimal("10086");
        System.out.println("BigDecimal对象1为：" + d);
        System.out.println("BigDecimal对象2为：" + e);
        BigValue(d, e);
        System.out.println("---------------------------------");

        //         - 创建一个方法，比较两个 `BigDecimal` 对象的值大小
        BigDecimal f = new BigDecimal(100);
        BigDecimal g = new BigDecimal(200);
        System.out.println("BigDecimal对象1为：" + f);
        System.out.println("BigDecimal对象2为：" + g);
        BigNumber(f, g);
        System.out.println("---------------------------------");
    }

    /**
     * （判断最小值和最大值）的方法
     */
    private static String Number(int n1, int n2, int n3) {
        int max = Math.max(n1, n2);
        int max1 = Math.max(max, n3);

        int min = Math.min(n1, n2);
        int min1 = Math.min(min, n3);

        return "最大值是：" + max1 + "最小值是：" + min1;
    }

    /**
     * （求平均值）的方法
     */
    private static double Avg(int[] arr) {
        int num = 0;
        for (int i = 0; i < arr.length; i++) {
            num += arr[i];
        }
        double avg = num / arr.length;
        return avg;
    }

    /**
     * （获取时间）的方法
     */
    private static void Time(Date date) {
        System.out.println(date.getYear() + "年" + date.getMonth() + "月" + date.getDay() + "日");
    }


    /**
     * （基本算术运算）的方法
     */
    private static void Big(BigDecimal a, BigDecimal b) {
        System.out.println(a + "和" + b + "的和是：" + a.add(b));
        System.out.println(a + "和" + b + "的差是：" + a.subtract(b));
        System.out.println(a + "和" + b + "的积是：" + a.multiply(b));
        System.out.println(a + "和" + b + "的商是：" + a.divide(b));
    }

    /**
     * （判断值是否相等）的方法
     */
    private static void BigValue(BigDecimal d, BigDecimal e) {
        System.out.println(d + " 和 " + e + "是否相等：" + d.equals(e));
    }

    /**
     * （返回字符串长度）的方法
     */
    private static void BigNumber(BigDecimal f, BigDecimal g) {
        int result = f.compareTo(g);
        if (result < 0) {
            System.out.println(f + " < " + g);
        } else if (result == 0) {
            System.out.println(f + " = " + g);
        } else {
            System.out.println(f + " > " + g);
        }
    }

    /**
     * （判断值大小）的方法
     */
    private static int StringSize(String s) {
        return s.length();
    }

    /**
     * （字符串反转）的方法
     */
    private static void StringReverse(String s) {
        System.out.println("字符串反转前：" + s);
        StringBuilder st = new StringBuilder(s);
        System.out.println("字符串反转后：" + st.reverse());
    }

    /**
     * （字符串大小写转换）的方法
     */
    private static void StringLowerUp(String s) {
        System.out.println(s + "全部转成大写：" + s.toUpperCase());
        System.out.println(s + "全部转成小写：" + s.toLowerCase());
    }

    /**
     * （字符串替换成指定字符串）的方法
     */
    private static String StringReplace(String s) {
        return s.replace("字符串", "字符");
    }

    /**
     * （去除字符串两端的空白）的方法
     */
    private static String StringTrim(String s) {
        return s.trim();
    }

    /**
     * （提取一个字符串中指定范围内的子串）的方法
     */
    private static String StringInto(String s, int i) {
        return s.substring(0, i);
    }

    /**
     * （字符串是否以特定的前缀开始）的方法
     */
    private static boolean StringStar(String s) {
        return s.startsWith("我");
    }

    /**
     * （字符串转换为相应的整数或浮点数）的方法
     */
    private static void StringtoNumbers(String s) {
        int i = Integer.parseInt(s);
        double j = Double.parseDouble(s);
        System.out.println("字符串转换为相应的整数：" + i + "，字符串转换为相应的浮点数：" + j);

    }


    }

