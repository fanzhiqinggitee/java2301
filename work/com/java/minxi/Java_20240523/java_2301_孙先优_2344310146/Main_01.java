package com.java.minxi.Java_20240523.java_2301_孙先优_2344310146;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Main_01 {


    public static void main(String[] args) throws Exception {

            Connection connection = null;
            Statement statement = null;
            ResultSet resultSet = null;

            //1、加载驱动包
            Class.forName("com.mysql.cj.jdbc.Driver");

            //2、获取MySQL链接
            String url = "jdbc:mysql://127.0.0.1:3306/student?serverTimezone=Asia/Shanghai";
            String userName = "root";
            String password = "031002";
            connection = DriverManager.getConnection(url, userName, password);

            //3、定义sql
            String sql = "select * from student_test";

            //4、获取执行sql的对象
            statement = connection.createStatement();

            //5、执行sql
            ResultSet rs = statement.executeQuery(sql);

            //6、处理结果
            while (rs.next()) {
                int id = rs.getInt("id");
                String studentName = rs.getString("student_name");
                int studentAge = rs.getInt("student_age");
                int sex = rs.getInt("student_sex");
                int studentGrades = rs.getInt("student_grades");

                System.out.print(id + " ");
                System.out.print(studentName + " ");
                System.out.print(studentAge + " ");
                System.out.print(sex + " ");
                System.out.println(studentGrades + " ");
            }
            //7、释放资源
            connection.close();
            statement.close();
            resultSet.close();
        }

        }



