package com.java.minxi.Java_20240523.java_2301_孙先优_2344310146;

import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Main_03 {
    public static void main(String[] args)throws Exception {




        ResultSet rs=null;
        Connection conn=null;
        Statement stmt =null;
        //加载配置文件
        try{
            Properties prop = new Properties();
            prop.load(new FileInputStream("work\\com\\java\\minxi\\Java_20240523\\java_2301_孙先优_2344310146/druid.properties"));

            //获取连接池对象
            DataSource dataSource = DruidDataSourceFactory.createDataSource(prop);

            //获取数据库链接
             conn= dataSource.getConnection();

            //定义sql
            String sql = "select * from student_test;";

            //获取对象
            PreparedStatement pstmt = conn.prepareStatement(sql);

            //执行sql
             rs = pstmt.executeQuery();

            //处理结果
            Student_Main sm=null;
            List<Student_Main> sm_1= new ArrayList<>();
            while (rs.next()){
                int id = rs.getInt("id");
                String studentName = rs.getString("student_name");
                int studentAge = rs.getInt("student_age");
                int studentSex = rs.getInt("student_sex");
                int studentGrades = rs.getInt("student_grades");

                //装在Student_Main对象
                 sm = new Student_Main();
                sm.setId(id);
                sm.setStudent_name(studentName);
                sm.setStudent_age(studentAge);
                sm.setStudent_sex(studentSex);
                sm.setStudent_grades(studentGrades);

                //装在集合
               sm_1.add(sm);

            }
            System.out.println(sm_1);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                if (null!=rs){
                    rs.close();
                }if (null!=conn){
                    conn.close();
                }if (null!=stmt){
                    stmt.close();
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
