package com.java.minxi.Java_20240523.java_2301_孙先优_2344310146;

import java.sql.*;

public class Main_02 {
    public static void main(String[] args) {
        try {
            Demo1();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void Demo1() throws Exception {
        ResultSet resultSet = null;
        Connection connection = null;
        Statement statement = null;
        try {
            String url = "jdbc:mysql://127.0.0.1:3306/student?serverTimezone=Asia/Shanghai";
            String userName = "root";
            String password = "031002";
            Connection conn = DriverManager.getConnection(url, userName, password);
            //定义SQL
            String sql = "select * from student_test where id=? and student_name = ?";


            //获取pstmt对象
            PreparedStatement pstmt = conn.prepareStatement(sql);

            //设置？的值
            pstmt.setInt(1,3 );
            pstmt.setString(2, "王麻子");

            //执行sql
            ResultSet rs = pstmt.executeQuery();

            //执行结果
            while (rs.next()) {
                int id = rs.getInt("id");
                String studentName = rs.getString("student_name");
                int studentAge = rs.getInt("student_age");
                int sex = rs.getInt("student_sex");
                int studentGrades = rs.getInt("student_grades");

                System.out.print(id + " ");
                System.out.print(studentName + " ");
                System.out.print(studentAge + " ");
                System.out.print(sex + " ");
                System.out.println(studentGrades + " ");
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            try {
                if (null != statement) {
                    statement.close();
                }
                if (null != connection) {
                    connection.close();
                }
                if (null != resultSet) {
                    resultSet.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}