package com.java.minxi.Java_20240523.java_2301_孙先优_2344310146;

public class Student_Main {
  private int id;
  private String student_name;
  private int student_age;
  private int student_sex;
  private int student_grades;

    public Student_Main() {
    }

    public Student_Main(int id, String student_name, int student_age, int student_sex, int student_grades) {
        this.id = id;
        this.student_name = student_name;
        this.student_age = student_age;
        this.student_sex = student_sex;
        this.student_grades = student_grades;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public int getStudent_age() {
        return student_age;
    }

    public void setStudent_age(int student_age) {
        this.student_age = student_age;
    }

    public int getStudent_sex() {
        return student_sex;
    }

    public void setStudent_sex(int student_sex) {
        this.student_sex = student_sex;
    }

    public int getStudent_grades() {
        return student_grades;
    }

    public void setStudent_grades(int student_grades) {
        this.student_grades = student_grades;
    }

    @Override
    public String toString() {
        return "Student_Main{" +
                "id=" + id +
                ", student_name='" + student_name + '\'' +
                ", student_age=" + student_age +
                ", student_sex=" + student_sex +
                ", student_grades=" + student_grades +
                '}';
    }


}
