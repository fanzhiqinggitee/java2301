package com.java.minxi.java_20240425.java_2301_孙先优_2344310146;

public class YuanGong {
    //    1.创建员工类，员工类中包含员工ID（Long类型）、账号（String类型）、密码（String类型）、
//    姓名（String类型）、年龄（int类型）属性；

   private Long ID;//员工ID
    private String zhanghao;//账号
    private String mima;//密码
    private String xingming;//姓名
    private int nianling;//年龄

    //            2.创建员工类的无参及有参构造；
    public YuanGong() {

    }
    public YuanGong(Long ID,String zhanghao,String mima,String xingming,int nianling){
        this.ID = ID;
        this.zhanghao = zhanghao;
        this.mima = mima;
        this.xingming = xingming;
        this.nianling = nianling;
    }

    //            3.使用封装思想，将员工类的变量进行封装，并提供对外访问的方式；
    public void setID(Long ID){
        this.ID = ID;
    }
    public Long getID(){
        return ID;
    }
    public void setZhangHao(String zhanghao){
        this.zhanghao = zhanghao;
    }
    public String getZhangHao(){
        return zhanghao;
    }
    public void setMiMa(String mima){
        this.mima = mima;
    }
    public String getMiMa(){
        return mima;
    }
    public void setXingming(String xingming){
        this.xingming =  xingming;
    }
    public String getXingMing(){
        return xingming;
    }
    public void setNianLing(int nianling){
     this.nianling = nianling;
    }
    public int getNianLing(){
        return nianling;
    }


}
