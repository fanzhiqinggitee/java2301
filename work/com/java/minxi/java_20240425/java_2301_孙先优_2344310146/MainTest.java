package com.java.minxi.java_20240425.java_2301_孙先优_2344310146;

import java.util.Scanner;

public class MainTest {
    static YuanGong yg = new YuanGong(1L, "student", "123456", "王麻子", 20);

    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        //            4.通过Java方法实现员工登录，上班打卡及退出系统三个功能
//            （最终结果在控制台输出，比如：登录成功，打卡成功，退出成功）；
//            1）要求员工的登录必须要进行账号密码校验，校验OK才可以登录；
//            2）上班打卡功能需要校验员工的ID和密码才可以进行打卡；
//            3）员工的退出，不需要校验，直接退出系统；
        //登录
        System.out.println("员工管理系统");
        while (true) {
            System.out.println("登录页面");
            System.out.println("1登录");
            System.out.println("2退出");
            int number = sc.nextInt();
            sc.nextLine();
            switch (number) {
                case 1:
                    login();
                    break;
                case 2:
                    System.out.println("退出登录");
                    return;
                default:
                    System.out.println("不规范输入");
                    break;

            }

        }


        }
        //打卡
        public static void login(){
            System.out.println("请输入您的账号");
          String zhanghao = sc.nextLine();
            System.out.println("请输入您的密码");
            String mima = sc.nextLine();
            if (yg.getZhangHao().equals(zhanghao)&&yg.getMiMa().equals(mima)){
                System.out.println("登录成功");
                while (true) {
                    System.out.println("1打卡");
                    System.out.println("2退出");
                    System.out.println("请选择你的操作");
                    int number = sc.nextInt();
                    switch (number){
                        case 1:
                            test();
                            break;
                        case 2:
                            System.out.println("退出打卡");
                            return;
                        default:
                            System.out.println("不规范输入");
                            break;
                    }
                }
            }else {
                System.out.println("账号或密码错误");
            }
        }
        public static void test(){
            System.out.println("请输入您的ID");
            Long ID = sc.nextLong();
            sc.nextLine();
            System.out.println("请输入您的密码");
            String mima1 = sc.nextLine();
            if (yg.getID().equals(ID)&&yg.getMiMa().equals(mima1)){
                System.out.println("打卡成功");
            }else {
                System.out.println("打卡失败");
            }
        }
    }
